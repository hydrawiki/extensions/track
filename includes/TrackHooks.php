<?php
/**
 * Wikia Inc.
 * Track
 *
 * @package   Track
 * @author    Garth Webb
 * @copyright (c) 2011 Wikia Inc.
 * @license   GPL-2.0-or-later
 * @link      https://www.wikia.com/
 **/

namespace Wikia\App\Extension\Track;

use OutputPage;
use SkinTemplate;

class TrackHooks {
	/**
	 * Add tracking Javascript to page.
	 *
	 * @param OutputPage   $outputPage
	 * @param SkinTemplate $skin
	 *
	 * @return void
	 */
	public static function onBeforePageDisplay(OutputPage $outputPage, SkinTemplate $skin) {
		$outputPage->addModules(['ext.track.scripts']);
	}
}
