/**
 * getCookieValue returns the value of a cookie
 *
 * @param {string} cookieName The name of the cookie to retrieve the value of
 * @returns {string | null} The value of the cookie, or null if the cookie is not set.
 */
function getCookieValue(cookieName) {
	var cookieSplit = ('; ' + document.cookie).split('; ' + cookieName + '=');
	return cookieSplit.length === 2 ? cookieSplit.pop().split(';').shift() : null;
}

/**
 * genUID generates a v4 UUID
 *
 * @returns {string} The UUID
 */
function genUID() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0,
			v = c === 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}

var script = document.createElement('script'),
	utma = getCookieValue('__utma'),
	utmb = getCookieValue('__utmb'),
	pvUID = genUID(),
	pvNumber = +getCookieValue('pv_number'),
	pvNumberGlobal = +getCookieValue('pv_number_global');

pvNumber = pvNumber ? parseInt(pvNumber, 10) + 1 : 1;
pvNumberGlobal = pvNumberGlobal ? parseInt(pvNumberGlobal, 10) + 1 : 1;

window.pvUID = pvUID;
window.pvNumber = pvNumber;
window.pvNumberGlobal = pvNumberGlobal;

var trackUrl = new URL("https://beacon.wikia-services.com/__track/view");
var parameters = trackUrl.searchParams;
parameters.set('cb', '' +
	Math.floor(Math.random() * 100000000) +
	Math.floor(Math.random() * 100000000));
if (mw.config.get('dsSiteKey')) {
	parameters.set('ck', mw.config.get('dsSiteKey'));
}
parameters.set('lc', mw.config.get('wgContentLanguage'));
parameters.set('lu', mw.config.get('wgUserLanguage'));
parameters.set('x', mw.config.get('wgDBname'));
parameters.set('a', mw.config.get('wgArticleId'));
parameters.set('s', mw.config.get('skin'));
parameters.set('pg', mw.config.get('wgPageName'));
parameters.set('n', mw.config.get('wgNamespaceNumber'));
if (typeof document.referrer != "undefined") {
	parameters.set('r', document.referrer);
}
if (utma && utma[1]) {
	parameters.set('utma', utma[1]);
}
if (utmb && utmb[1]) {
	parameters.set('utmb', utmb[1]);
}
parameters.set('pv_unique_id', pvUID);
parameters.set('pv_number', pvNumber);
parameters.set('pv_number_global', pvNumberGlobal);
parameters.set('u', mw.config.get('wgUserGlobalId'));
parameters.set('p', 'hydra');

// Use the Resource Timing API to get statistics about page load performance.
if (performance && performance.getEntriesByType) {
	const navTimings = performance.getEntriesByType('navigation');
	if (navTimings && navTimings.length) {
		// Though there is usually only one, take the most recent
		// one if there are multiple.
		/** @type {PerformanceNavigationTiming} */
		const t = navTimings[navTimings.length - 1];
		const s = t.startTime;
		// Time to first byte -- time in ms it takes for DNS lookup,
		// TCP connection setup, and TLS setup.
		parameters.set('ttfb', Math.round(t.responseStart - s));
		// Time to last byte -- time in ms for the page response to
		// finish.
		parameters.set('ttlb', Math.round(t.responseEnd - s));
		// Time to dom interactive -- time in ms until the user can
		// view the page and scroll or click.
		parameters.set('ttdi', Math.round(t.domInteractive - s));
		// Time to server response -- time in ms between connection end
		// and response start.
		parameters.set('ttsr', Math.round(t.responseStart - t.connectEnd));
		// Transfer size in bytes
		parameters.set('tsb', t.transferSize);
	}
}

script.src = trackUrl;
script.onload = function() {
	var expireDate = new Date(),
		beacon_id = window.beacon_id ? window.beacon_id : getCookieValue('wikia_beacon_id'),
		session_id = window.session_id ? window.session_id : getCookieValue('tracking_session_id'),
		cookieDomain = mw.config.get('MercuryAuthCookieDomain');

	expireDate = new Date(expireDate.getTime() + 1000 * 60 * 30);
	document.cookie = 'wikia_beacon_id=' + beacon_id + '; expires=' + expireDate.toGMTString() +
		';domain=' + cookieDomain + '; path=' + mw.config.get('wgCookiePath') + ';';
	document.cookie = 'tracking_session_id=' + session_id + '; expires=' + expireDate.toGMTString() +
		';domain=' + cookieDomain + '; path=' + mw.config.get('wgCookiePath') + ';';
	document.cookie = 'pv_number=' + pvNumber + '; expires=' + expireDate.toGMTString() +
		'; path=' + mw.config.get('wgCookiePath') + ';';
	document.cookie = 'pv_number_global=' + pvNumberGlobal + '; expires=' + expireDate.toGMTString() +
		';domain=' + cookieDomain + '; path=' + mw.config.get('wgCookiePath') + ';';
};

document.head.appendChild(script);
